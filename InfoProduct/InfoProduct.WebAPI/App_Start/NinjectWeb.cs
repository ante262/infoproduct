[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(InfoProduct.WebAPI.App_Start.NinjectWeb), "Start")]

namespace InfoProduct.WebAPI.App_Start
{
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject.Web;
    using Ninject.Web.Common;

    public static class NinjectWeb
    {
        #region Methods

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
        }

        #endregion Methods
    }
}