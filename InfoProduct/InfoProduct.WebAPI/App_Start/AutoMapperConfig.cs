﻿using AutoMapper;
using InfoProduct.Model;
using InfoProduct.Model.Common;
using InfoProduct.WebAPI.Controllers;

namespace InfoProduct.WebAPI.App_Start
{
    public static class AutoMapperConfig
    {
        #region Methods

        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                // Model
                cfg.AddProfile<AutoMapperMaps.ModelProfile>();

                // CategoryController
                cfg.CreateMap<CategoryController.CategoryModel, CategoryPOCO>().ReverseMap();
                cfg.CreateMap<CategoryController.CategoryModel, ICategory>().ReverseMap();

                // ProductContorller
                cfg.CreateMap<ProductController.ProductModel, ProductPOCO>().ReverseMap();
                cfg.CreateMap<ProductController.ProductModel, IProduct>().ReverseMap();
            });
        }

        #endregion Methods
    }
}