﻿using AutoMapper;
using InfoProduct.Model.Common;
using InfoProduct.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace InfoProduct.WebAPI.Controllers
{
    [RoutePrefix("api/Product")]
    public class ProductController : ApiController
    {
        #region Constructors

        public ProductController(IProductService service)
        {
            Service = service;
        }

        #endregion Constructors

        #region Properties

        private IProductService Service { get; set; }

        #endregion Properties

        #region Methods

        // DELETE: api/Product/
        [HttpDelete]
        [Route("{Id}")]
        public async Task<IHttpActionResult> Delete(Guid Id)
        {
            try
            {
                var result = await Service.DeleteAsync(Id);
                if (result == 1) return Ok("Deleted");
                else return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        // GET: api/Product
        [HttpGet]
        [Route("")]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await Service.GetAsync();
                if (result != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,
                        Mapper.Map<List<ProductModel>>(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        // GET: api/Product/5
        [HttpGet]
        [Route("{Id}")]
        public async Task<HttpResponseMessage> Get(Guid Id)
        {
            var result = await Service.GetAsync(Id);

            if (result != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ProductModel>(result));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Product
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post(ProductModel entity)
        {
            entity.Id = Guid.NewGuid();

            try
            {
                var result = await Service.InsertAsync(Mapper.Map<IProduct>(entity));

                if (result == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Add operation error.");

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // PUT: api/Product/5
        [HttpPut]
        [Route("{Id}")]
        public async Task<HttpResponseMessage> Put(Guid ID, ProductModel entity)
        {
            try
            {
                if (ID != entity.Id)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                        "Ids do not match.");
                }

                var result = await Service.UpdateAsync(Mapper.Map<IProduct>(entity));
                if (result == 1)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError,
                        "PUT unsuccessful.");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
            }
        }

        #endregion Methods

        #region Classes

        public class ProductModel
        {
            #region Properties

            public Guid Id { get; set; }

            [Required]
            [StringLength(256)]
            public string Name { get; set; }

            public Guid CategoryId { get; set; }

            public decimal Price { get; set; }

            #endregion Properties
        }

        #endregion Classes
    }
}