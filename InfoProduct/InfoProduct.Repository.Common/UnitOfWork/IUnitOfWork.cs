﻿using System;
using System.Threading.Tasks;

namespace InfoProduct.Repository.Common
{
    public interface IUnitOfWork : IDisposable
    {
        #region Methods

        Task<int> AddAsync<T>(T entity) where T : class;

        Task<int> CommitAsync();

        Task<int> DeleteAsync<T>(T entity) where T : class;

        Task<int> DeleteAsync<T>(Guid Id) where T : class;

        Task<int> UpdateAsync<T>(T entity) where T : class;

        #endregion Methods
    }
}