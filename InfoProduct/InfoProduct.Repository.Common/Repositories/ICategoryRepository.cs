﻿using InfoProduct.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfoProduct.Repository.Common
{
    public interface ICategoryRepository
    {
        #region Methods

        Task<int> DeleteAsync(ICategory entity);

        Task<int> DeleteAsync(Guid Id);

        Task<List<ICategory>> GetAsync();

        Task<ICategory> GetAsync(Guid Id);

        Task<int> InsertAsync(ICategory entity);

        Task<int> UpdateAsync(ICategory entity);

        #endregion Methods
    }
}