﻿namespace InfoProduct.Repository.Common
{
    public interface IRepositoryFactory
    {
        #region Methods

        IProductRepository CreateProductRepository();

        ICategoryRepository CreateCategoryRepository();

        #endregion Methods
    }
}