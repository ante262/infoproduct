﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace InfoProduct.Repository.Common
{
    public interface IRepository
    {
        IUnitOfWork CreateUnitOfWork();

        Task<int> AddAsync<T>(T entity) where T : class;

        Task<int> DeleteAsync<T>(T entity) where T : class;

        Task<int> DeleteAsync<T>(Guid Id) where T : class;

        Task<int> InsertAsync<T>(T entity) where T : class;

        Task<T> SingleAsync<T>(Guid Id) where T : class;

        Task<int> UpdateAsync<T>(T entity) where T : class;

        IQueryable<T> WhereAsync<T>() where T : class;
    }
}