﻿using Ninject.Extensions.Factory;
using Ninject.Modules;
using InfoProduct.DAL;
using InfoProduct.Repository.Common;

using System;

namespace InfoProduct.Repository
{
    public class DIModule : NinjectModule
    {
        #region Methods

        public override void Load()
        {
            Bind<IInfoProductContext>().To<DBModel>();
            Bind<IRepository>().To<Repository>();
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<IUnitOfWorkFactory>().ToFactory();
            Bind<IRepositoryFactory>().ToFactory();

            Bind<IProductRepository>().To<ProductRepository>();
            Bind<ICategoryRepository>().To<CategoryRepository>();
        }

        #endregion Methods
    }
}