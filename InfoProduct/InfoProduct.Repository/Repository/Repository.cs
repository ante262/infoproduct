﻿using InfoProduct.DAL;
using InfoProduct.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace InfoProduct.Repository
{
    public class Repository : IRepository
    {
        #region Constructors

        public Repository(IInfoProductContext dbContext, IUnitOfWorkFactory unitOfWorkFactory)
        {
            DbContext = dbContext ?? throw new ArgumentNullException("DbContext");
            UnitOfWorkFactory = unitOfWorkFactory;
        }

        #endregion Constructors

        #region Properties

        protected IInfoProductContext DbContext { get; private set; }

        protected IUnitOfWorkFactory UnitOfWorkFactory { get; private set; }

        #endregion Properties

        #region Methods

        public async Task<int> AddAsync<T>(T entity) where T : class
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbContext.Set<T>().Add(entity);
            }
            return await DbContext.SaveChangesAsync();
        }

        public IUnitOfWork CreateUnitOfWork()
        {
            return UnitOfWorkFactory.CreateUnitOfWork();
        }

        public async Task<int> DeleteAsync<T>(T entity) where T : class
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbContext.Set<T>().Attach(entity);
                DbContext.Set<T>().Remove(entity);
            }
            return await DbContext.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync<T>(Guid Id) where T : class
        {
            var entity = await SingleAsync<T>(Id);
            if (entity == null)
            {
                throw new KeyNotFoundException("Entity with specified Id not found.");
            }
            return await DeleteAsync<T>(entity);
        }

        public async Task<int> InsertAsync<T>(T entity) where T : class
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbContext.Set<T>().Add(entity);
            }
            return await DbContext.SaveChangesAsync();
        }

        public Task<T> SingleAsync<T>(Guid Id) where T : class
        {
            return DbContext.Set<T>().FindAsync(Id);
        }

        public async Task<int> UpdateAsync<T>(T entity) where T : class
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbContext.Set<T>().Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
            return await DbContext.SaveChangesAsync();
        }

        public IQueryable<T> WhereAsync<T>() where T : class
        {
            return DbContext.Set<T>().AsNoTracking();
        }

        #endregion Methods
    }
}