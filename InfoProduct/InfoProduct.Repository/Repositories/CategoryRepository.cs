﻿using AutoMapper;
using InfoProduct.DAL;
using InfoProduct.Model.Common;
using InfoProduct.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace InfoProduct.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        #region Constructors

        public CategoryRepository(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public virtual Task<int> DeleteAsync(ICategory entity)
        {
            return Repository.DeleteAsync<Category>(Mapper.Map<Category>(entity));
        }

        public virtual Task<int> DeleteAsync(Guid Id)
        {
            return Repository.DeleteAsync<Category>(Id);
        }

        public virtual async Task<List<ICategory>> GetAsync()
        {
            return Mapper.Map<List<ICategory>>(
                await Repository.WhereAsync<Category>()
                        .ToListAsync<Category>()
                );
        }

        public virtual async Task<ICategory> GetAsync(Guid Id)
        {
            return Mapper.Map<ICategory>(await Repository.SingleAsync<Category>(Id));
        }

        public virtual Task<int> InsertAsync(ICategory entity)
        {
            return Repository.InsertAsync<Category>(Mapper.Map<Category>(entity));
        }

        public virtual Task<int> UpdateAsync(ICategory entity)
        {
            return Repository.UpdateAsync<Category>(Mapper.Map<Category>(entity));
        }

        #endregion Methods
    }
}