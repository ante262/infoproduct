﻿using AutoMapper;
using InfoProduct.DAL;
using InfoProduct.Model.Common;
using InfoProduct.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace InfoProduct.Repository
{
    public class ProductRepository : IProductRepository
    {
        #region Constructors

        public ProductRepository(IRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public virtual Task<int> DeleteAsync(IProduct entity)
        {
            return Repository.DeleteAsync<Product>(Mapper.Map<Product>(entity));
        }

        public virtual Task<int> DeleteAsync(Guid Id)
        {
            return Repository.DeleteAsync<Product>(Id);
        }

        public virtual async Task<List<IProduct>> GetAsync()
        {
            return Mapper.Map<List<IProduct>>(
                await Repository.WhereAsync<Product>()
                        .ToListAsync<Product>()
                );
        }

        public virtual async Task<IProduct> GetAsync(Guid Id)
        {
            return Mapper.Map<IProduct>(await Repository.SingleAsync<Product>(Id));
        }

        public virtual Task<int> InsertAsync(IProduct entity)
        {
            return Repository.InsertAsync<Product>(Mapper.Map<Product>(entity));
        }

        public virtual Task<int> UpdateAsync(IProduct entity)
        {
            return Repository.UpdateAsync<Product>(Mapper.Map<Product>(entity));
        }

        #endregion Methods
    }
}