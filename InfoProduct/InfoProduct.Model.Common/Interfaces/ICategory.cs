﻿using System;

namespace InfoProduct.Model.Common
{
    public interface ICategory
    {
        Guid Id { get; set; }

        string Name { get; set; }
    }
}