﻿using System;

namespace InfoProduct.Model.Common
{
    public interface IProduct
    {
        Guid Id { get; set; }

        string Name { get; set; }

        decimal Price { get; set; }

        Guid CategoryId { get; set; }
    }
}