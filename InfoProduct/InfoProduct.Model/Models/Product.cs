﻿using InfoProduct.Model.Common;
using System;

namespace InfoProduct.Model
{
    public partial class ProductPOCO : IProduct
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public Guid CategoryId { get; set; }
    }
}