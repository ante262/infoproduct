﻿using InfoProduct.Model.Common;
using System;

namespace InfoProduct.Model
{
    public partial class CategoryPOCO : ICategory
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}