﻿using InfoProduct.Model.Common;
using Ninject.Modules;

namespace InfoProduct.Model
{
    public class DIModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICategory>().To<CategoryPOCO>();
            Bind<IProduct>().To<ProductPOCO>();
        }
    }
}