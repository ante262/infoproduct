﻿using AutoMapper;
using AutoMapper.Configuration;
using InfoProduct.DAL;
using InfoProduct.Model.Common;
using System;

namespace InfoProduct.Model
{
    public static class AutoMapperMaps
    {
        public class ModelProfile : Profile
        {
            public ModelProfile()
            {
                CreateMap<CategoryPOCO, Category>().ReverseMap();
                CreateMap<ICategory, Category>().ReverseMap();
                CreateMap<CategoryPOCO, ICategory>().ReverseMap();

                CreateMap<ProductPOCO, Product>().ReverseMap();
                CreateMap<IProduct, Product>().ReverseMap();
                CreateMap<ProductPOCO, IProduct>().ReverseMap();
            }
        }
    }
}