﻿using Ninject.Modules;
using InfoProduct.Service.Common;

using System;

namespace InfoProduct.Service
{
    public class DIModule : NinjectModule
    {
        #region Methods

        public override void Load()
        {
            Bind<ICategoryService>().To<CategoryService>();
            Bind<IProductService>().To<ProductService>();
        }

        #endregion Methods
    }
}