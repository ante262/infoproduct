﻿using InfoProduct.Model.Common;
using InfoProduct.Repository.Common;
using InfoProduct.Service.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfoProduct.Service
{
    public class ProductService : IProductService
    {
        #region Constructors

        public ProductService(IProductRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected IProductRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(IProduct entity)
        {
            return Repository.DeleteAsync(entity);
        }

        public Task<int> DeleteAsync(Guid Id)
        {
            return Repository.DeleteAsync(Id);
        }

        public Task<List<IProduct>> GetAsync()
        {
            return Repository.GetAsync();
        }

        public Task<IProduct> GetAsync(Guid Id)
        {
            return Repository.GetAsync(Id);
        }

        public Task<int> InsertAsync(IProduct entity)
        {
            return Repository.InsertAsync(entity);
        }

        public Task<int> UpdateAsync(IProduct entity)
        {
            return Repository.UpdateAsync(entity);
        }

        #endregion Methods
    }
}