﻿using InfoProduct.Model.Common;
using InfoProduct.Repository.Common;
using InfoProduct.Service.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfoProduct.Service
{
    public class CategoryService : ICategoryService
    {
        #region Constructors

        public CategoryService(ICategoryRepository repository)
        {
            Repository = repository;
        }

        #endregion Constructors

        #region Properties

        protected ICategoryRepository Repository { get; private set; }

        #endregion Properties

        #region Methods

        public Task<int> DeleteAsync(ICategory entity)
        {
            return Repository.DeleteAsync(entity);
        }

        public Task<int> DeleteAsync(Guid Id)
        {
            return Repository.DeleteAsync(Id);
        }

        public Task<List<ICategory>> GetAsync()
        {
            return Repository.GetAsync();
        }

        public Task<ICategory> GetAsync(Guid Id)
        {
            return Repository.GetAsync(Id);
        }

        public Task<int> InsertAsync(ICategory entity)
        {
            return Repository.InsertAsync(entity);
        }

        public Task<int> UpdateAsync(ICategory entity)
        {
            return Repository.UpdateAsync(entity);
        }

        #endregion Methods
    }
}