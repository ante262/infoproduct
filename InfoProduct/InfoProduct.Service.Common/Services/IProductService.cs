﻿using InfoProduct.Model.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfoProduct.Service.Common
{
    public interface IProductService
    {
        #region Methods

        Task<int> DeleteAsync(IProduct entity);

        Task<int> DeleteAsync(Guid Id);

        Task<List<IProduct>> GetAsync();

        Task<IProduct> GetAsync(Guid Id);

        Task<int> InsertAsync(IProduct entity);

        Task<int> UpdateAsync(IProduct entity);

        #endregion Methods
    }
}