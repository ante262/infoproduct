﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace InfoProduct.DAL
{
    public interface IInfoProductContext : IDisposable
    {
        #region Properties

        DbSet<Category> Category { get; set; }

        DbSet<Product> Product { get; set; }

        #endregion Properties

        #region Methods

        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        Task<int> SaveChangesAsync();

        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        #endregion Methods
    }
}